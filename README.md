## Run Back-End
```sh
$ npm install
$ npm start
```
Back-End would be accessible through http://localhost:5000

## Run Front-End
```sh
$ cd client
$ npm install
$ npm start
```
Front-End would be accessible through http://localhost:4443
