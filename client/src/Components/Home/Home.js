import React, { useContext, useEffect } from "react";
import { SocketContext } from "../../SocketContext";
import "./Home.css";
import Footer from "../Footer/Footer";
import { Link } from "react-router-dom";
import { message } from "antd";
import Navbar from "../Navbar/Navbar";

const Home = (props) => {
  const paramsCode = props.location.search;

  const { meetingCode, setMeetingCode, setNewMeet } = useContext(SocketContext);

  useEffect(() => {
    if (paramsCode.length) {
      if (paramsCode.substring(0, 5) == "?ref=") return; // for product hunt ref
      setMeetingCode(paramsCode.substring(1));
    }
    setNewMeet(null);
  }, []);

  return (
    <div className="home">
      <Navbar />
      <div className="body-div">
        <div className="flex-box">
          <div className="left-div">
            <div
              className="contents"
              style={{
                margin: "10% 10% 0 10%",
                display: "flex",
                flexDirection: "column",
                alignItems: "flex-start",
                width: "500px",
              }}
            >
              <div
                className="start-meet"
                style={{ display: "flex", width: "100%" }}
              >
                <Link
                  className="home-btn"
                  to="join"
                  onClick={() => {
                    setNewMeet(true);
                  }}
                  style={{
                    display: "flex",
                    width: "100%",
                    justifyContent: "center",
                  }}
                >
                  Start Meeting
                </Link>
              </div>
              <div className="join-meet">
                <input
                  type="text"
                  placeholder="Enter meeting code"
                  value={meetingCode || ""}
                  onChange={(e) => {
                    setMeetingCode(e.target.value);
                  }}
                />
                <button
                  className="home-btn"
                  onClick={() => {
                    if (!meetingCode || meetingCode.trim().length === 0) {
                      message.error("Please enter the meeting code");
                      return;
                    }
                    props.history.push("join");
                  }}
                  style={{ margin: "unset" }}
                >
                  Join Meeting
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default Home;
