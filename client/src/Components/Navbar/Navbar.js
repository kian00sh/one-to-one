import React from "react";
import { APP_NAME } from "../../constants";
import "./Navbar.css";
import { useHistory } from "react-router-dom";

const Navbar = () => {
  const history = useHistory();
  return (
    <div className="navbar">
      <div className="title-div">
        <h3 onClick={() => history.push("/")} style={{ cursor: "pointer" }}>
          {APP_NAME}
        </h3>
      </div>
    </div>
  );
};

export default Navbar;
